package bst

class BinarySearchTreeIterator<T>(tree: BinarySearchTree<T>) : Iterator<T> {
    private val lifo = mutableListOf<BinarySearchTree.Node<T>>()
    private var nextElementIndex = 0

    init {
        addNodeAndAlOfItstLeftDescendantsToTheLifo(tree.root)
    }

    private fun addNodeAndAlOfItstLeftDescendantsToTheLifo(right: BinarySearchTree.Node<T>?) {
        var childNode = right
        while (childNode != null) {
            lifo.add(childNode)
            childNode = childNode.left
        }
    }

    override fun next(): T {
        val ret: T
        val node = lifo.last()
        ret = node.elements[nextElementIndex]
        nextElementIndex++

        if (nextElementIndex >= node.elements.size) {
            nextElementIndex = 0
            goToTheNextNode(node)
        }
        return ret
    }

    private fun goToTheNextNode(node: BinarySearchTree.Node<T>) {
        lifo.removeAt(lifo.lastIndex)
        addNodeAndAlOfItstLeftDescendantsToTheLifo(node.right)
    }

    override fun hasNext(): Boolean {
        return !lifo.isEmpty()
    }


//    private var elementIndex: Int = 0
//
//    override fun hasNext(): Boolean {
//        return elementIndex < tree.size
//    }
//
//    override fun next(): T {
//        val ret = tree[elementIndex]
//        elementIndex++
//        return ret
//    }
}