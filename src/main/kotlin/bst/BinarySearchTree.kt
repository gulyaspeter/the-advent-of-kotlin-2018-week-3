package bst

interface BinarySearchTree<T> {
    val size: Int
    val root: Node<T>?
    val comparator: Comparator<T>
    fun add(element: T)
    fun remove(element: T)

    interface Node<T> {
        val elements: List<T>
        val numberOfElementInSubtree: Int
        val left: Node<T>?
        val right: Node<T>?
    }
}