import bst.BinarySearchTree
import bst.BinarySearchTreeIterator
import bst.avltree.AvlTree
import bst.contains
import bst.get


fun <T : Comparable<T>> sortedMutableListOf(vararg elements: T): SortedMutableList<T> {
    val comparator = Comparator<T> { p0, p1 -> p0.compareTo(p1) }
    return SortedMutableListImpl(comparator, *elements)
}

fun <T> sortedMutableListOf(comparator: Comparator<T>, vararg elements: T): SortedMutableList<T> {
    return SortedMutableListImpl(comparator, *elements)

}

class SortedMutableListImpl<T>(comparator: Comparator<T>, vararg elements: T) : SortedMutableList<T> {
    private val tree: BinarySearchTree<T>

    init {
        tree = AvlTree(comparator, *elements)
    }

    override val size: Int
        get() = tree.size

    override fun add(element: T) = tree.add(element)
    override fun remove(element: T) = tree.remove(element)
    override fun get(index: Int): T = tree.get(index)
    override fun contains(element: T): Boolean = tree.contains(element)
    override fun iterator(): Iterator<T> = BinarySearchTreeIterator(tree)
}
